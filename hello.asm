section .data
    ;declaração e inicialização de dados
msg db 'Como programar em Assembly - Curso Assembly Progressivo', 0AH
len equ $-msg 

section .text 
global _start
    ;mostra onde o programa deve começar a executar
_start: mov edx, len 
        mov ecx, msg 
        mov ebx, 1
        mov eax, 4
        int 80h
        
        mov ebx, 0
        mov eax, 1
        int 80h
